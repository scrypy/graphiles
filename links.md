
# For customized atom

## Atom Dev
* [intro to hacking atom](https://revelry.co/diy-atom-an-intro-to-hacking-your-text-editor/)
* [atom packages in pure JS](https://www.sitepoint.com/write-atom-packages-using-vanilla-javascript/)
* [Init file](http://flight-manual.atom.io/hacking-atom/sections/the-init-file/)
* [Custom Theme](https://webdesign.tutsplus.com/tutorials/how-to-create-a-custom-theme-for-atom--cms-20878)
* [Creating Snippets](http://ursooperduper.github.io/2015/11/16/creating-snippets-for-atom.html)
* [Plugin Dev](https://github.com/blog/2231-building-your-first-atom-plugin)
* [Creating a package](https://blog.eleven-labs.com/en/create-atom-package/)
* [Plugin Dev2](https://discuss.atom.io/t/plugin-development/20403)
* [Package Dev Demo](https://www.sitepoint.com/how-to-write-a-syntax-highlighting-package-for-atom/)

## Packages
* [Rest Client](https://atom.io/packages/rest-client)
* [for academic writing](https://discuss.atom.io/t/using-atom-for-academic-writing/19222)
* [example file nav package](http://atom-packages.directory/category/navigation-tree-view/)
* [git log package](https://discuss.atom.io/t/git-log-package-graphs-your-commits/11908)
* [interactive diagram package](https://atom.io/packages/diagrams)
* [Hydrogen package (like jupyter)](https://nteract.gitbooks.io/hydrogen/)
